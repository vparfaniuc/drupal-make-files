# README #

* Repository to manage custom FuseIQ .makefile files
* Version - 1.3.0

## To run the make file ##

drush make https://bitbucket.org/vparfaniuc/drupal-make-files/raw/HEAD/FuseIQ-default.makefile [sitename]


## List of modules and theme included ##

### DEFAULT (FuseIQ-default.makefile) ###

Default FuseIQ set of modules compilation

#### MODULES ####

KEY MODULES 	

* Views (views)
* Date (date)
* Entity API (entity)
* Pathauto (pathauto)
* Pathologic (pathologic)
* Webform (webform)
* Chaos tool suite (ctools)
* Redirect (redirect)
* Global Redirect (globalredirect)

BACKEND UI

* Administration menu (admin_menu)
* Administration Menu select (admin_select)
* Adminimal Administration Menu (adminimal_admin_menu)
* Field Group (field_group)
* IMCE (imce)
* IMCE Wysiwyg bridge (imce_wysiwyg)
* Link (link)
* Linkit - Enriched linking experience (linkit)
* Linkit views (linkit_views)
* Module Filter (module_filter)
* Wysiwyg (wysiwyg)
* Entity reference (entityreference)
* Token (token)
* Permissions Grid (permission_grid)
* Views Bulk Operations (views_bulk_operations)
* Taxonomy Manager (taxonomy_manager)

THEMING

* Custom Breadcrumbs (custom_breadcrumbs)
* Menu block (menu_block)
* Nice Menus (nice_menus)
* Printer, email and PDF versions (print)
* Better Exposed Filters (better_exposed_filters)
* Fieldable Panels Panes (fieldable_panels_panes)
* Panels (panels)
* jQuery Update (jquery_update)
* Chosen (chosen)

DEVELOPMENT

* Devel (devel)
* Backup and Migrate (backup_migrate)
* Stage File Proxy (stage_file_proxy)

SECURITY

* Honeypot (honeypot)
* reCAPTCHA (recaptcha)
* Username Enumeration Prevention (username_enumeration_prevention)
* Password policy (password_policy)
* Security Kit (seckit)
* Login Security (Login Security)
* Password Policy (password_policy)

PERFORMANCE

* Views Litepager (views_litepager)
* Fast 404 (fast_404)
* HTML Purifier (htmlpurifier)

SEO & ANALYTICS

* Google Analytics (google_analytics)
* XML sitemap (xmlsitemap)
* Page Title (page_title)
* Search 404 (search404)
* Site verification (site_verify)

UTILITY

* Libraries API (libraries)
* Scheduler (scheduler)
* SMTP Authentication Support (smtp)

#### THEMES ####

* Adminimal - Responsive Administration Theme (adminimal_theme)
* Omega (omega)

#### JS LIBRARIES ####

* ckeditor
* chosen

## Contact ##

* MAINTAINER: Vadim Parfaniuc
* MAINTAINER CONTACT: vadim@fuseiq.com