; Drupal core and Drush make verion
core = 7.x
api = 2

; Include drupal core
projects[] = "drupal"

; KEY MODULES
projects[views][subdir] = "contrib"
projects[date][subdir] = "contrib"
projects[entity][subdir] = "contrib"
projects[pathauto][subdir] = "contrib"
projects[pathologic][subdir] = "contrib"
projects[webform][subdir] = "contrib"
projects[ctools][subdir] = "contrib"
projects[redirect][subdir] = "contrib"
projects[globalredirect][subdir] = "contrib"

; BACKEND UI
projects[admin_menu][subdir] = "contrib"
projects[admin_select][subdir] = "contrib"
projects[adminimal_admin_menu][subdir] = "contrib"
projects[field_group][subdir] = "contrib"
projects[imce][subdir] = "contrib"
projects[imce_wysiwyg][subdir] = "contrib"
projects[link][subdir] = "contrib"
projects[linkit][subdir] = "contrib"
projects[linkit_views][subdir] = "contrib"
projects[module_filter][subdir] = "contrib"
projects[wysiwyg][subdir] = "contrib"
projects[entityreference][subdir] = "contrib"
projects[token][subdir] = "contrib"
projects[permission_grid][subdir] = "contrib"
projects[views_bulk_operations][subdir] = "contrib"
projects[taxonomy_manager][subdir] = "contrib"

; THEMING
projects[custom_breadcrumbs][subdir] = "contrib"
projects[menu_block][subdir] = "contrib"
projects[nice_menus][subdir] = "contrib"
projects[print][subdir] = "contrib"
projects[better_exposed_filters][subdir] = "contrib"
projects[fieldable_panels_panes][subdir] = "contrib"
projects[panels][subdir] = "contrib"
projects[jquery_update][subdir] = "contrib"
projects[chosen][subdir] = "contrib"

; DEVELOPMENT
projects[devel][subdir] = "contrib"
projects[backup_migrate][subdir] = "contrib"
projects[stage_file_proxy][subdir] = "contrib"

; SECURITY
projects[honeypot][subdir] = "contrib"
projects[recaptcha][subdir] = "contrib"
projects[username_enumeration_prevention][subdir] = "contrib"
projects[password_policy][subdir] = "contrib"
projects[seckit][subdir] = "contrib"
projects[login_security][subdir] = "contrib"
projects[password_policy][subdir] = "contrib"

; PERFORMANCE
projects[views_litepager][subdir] = "contrib"
projects[fast_404][subdir] = "contrib"
projects[htmlpurifier][subdir] = "contrib"

; THEMES
projects[adminimal_theme][type] = "theme"
projects[omega][type] = "theme"

; SEO & ANALYTICS
projects[google_analytics][subdir] = "contrib"
projects[xmlsitemap][subdir] = "contrib"
projects[page_title][subdir] = "contrib"
projects[search404][subdir] = "contrib"
projects[site_verify][subdir] = "contrib"

; UTILITY
projects[libraries][subdir] = "contrib"
projects[scheduler][subdir] = "contrib"

; CKEDITOR 
libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.6.2/ckeditor_3.6.6.2.zip"
libraries[ckeditor][directory_name] = "ckeditor"

; CHOSEN
libraries[chosen][download][type] = "get"
libraries[chosen][download][url] = "https://github.com/harvesthq/chosen/releases/download/1.4.2/chosen_v1.4.2.zip"
libraries[chosen][directory_name] = "chosen"